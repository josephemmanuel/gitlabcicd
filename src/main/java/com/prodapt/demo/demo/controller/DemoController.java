package com.prodapt.demo.demo.controller;

import javax.websocket.server.PathParam;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoController {

	@GetMapping("/sayHello")
	public String sayHello() {
		return "Hello Hackathon..!";
	}
	
	@GetMapping("/sayHello/{name}")
	public String sayHello(@PathVariable("name") String name) {
		return "Hello "+name+", welcome to Hackathon.!!";
	}
	
}
